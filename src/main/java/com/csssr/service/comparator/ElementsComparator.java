package com.csssr.service.comparator;

import org.springframework.stereotype.Component;

import java.util.Comparator;

@Component
public class ElementsComparator implements Comparator<String> {
  @Override
  public int compare(String o1, String o2) {
    if (o1.length() == o2.length()) {
      return o1.compareTo(o2);
    }

    return o2.length() - o1.length();
  }
}
