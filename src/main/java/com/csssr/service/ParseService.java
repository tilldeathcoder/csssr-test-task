package com.csssr.service;

import com.csssr.service.model.ParseRequest;
import com.csssr.service.model.ParsedResult;

/**
 * This service parses the {@link ParseRequest} object.
 *
 * @author Eugene Makarenko
 * @see ParseRequest
 */
public interface ParseService {

  ParsedResult parse(ParseRequest parseRequest);
}
