package com.csssr.service.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * This class contains information for the string parsing.
 *
 * @author Eugene Makarenko
 * @see ParseSettings
 */
@ApiModel(
    value = "Request",
    description = "Set the settings, and the string which you want to be parsed")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ParseRequest {

  @ApiModelProperty(
      value =
          "The settings to parse string. If you provide regexp, it won't work. The parser splits the string by full matching of the separator. Can't be null or empty",
      required = true)
  @NotNull(message = "{notNullOrEmpty}")
  @Valid
  private ParseSettings settings;

  @ApiModelProperty(value = "The string to parse. Can't be null or empty", required = true)
  @NotBlank(message = "{notNullOrEmpty}")
  private String stringToParse;
}
