package com.csssr.service.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * This class presents the group of parsed elements.
 *
 * @author Eugene Makarenko
 */
@ApiModel(description = "The group contains the label and elements of the parsed string")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Group {

  @ApiModelProperty(value = "The label of the group")
  private char label;

  @ApiModelProperty(value = "The elements of the group")
  private List<String> elements;
}
