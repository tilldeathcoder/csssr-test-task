package com.csssr.web.controller.v1;

import com.csssr.service.ParseService;
import com.csssr.service.model.ParseRequest;
import com.csssr.service.model.ParsedResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * The REST controller to work with {@link ParseRequest} object.
 *
 * @author Eugene Makarenko
 * @see ParseRequest
 * @see ParseService
 */
@Api(tags = "Parse the string", description = "Parse the string using the settings")
@RestController
@RequestMapping(path = "api/", produces = "application/json", consumes = "application/json")
public class ParseController {

  private ParseService service;

  @Autowired
  public ParseController(ParseService service) {
    this.service = service;
  }

  @ApiOperation(value = "version: 1.0.0", response = ParsedResult.class)
  @ApiResponses(
      value = {
        @ApiResponse(code = 200, message = "Success"),
        @ApiResponse(code = 400, message = "Error"),
        @ApiResponse(code = 500, message = "Server error")
      })
  @PostMapping("v1/parse")
  public ResponseEntity<ParsedResult> parse(@RequestBody @Valid ParseRequest body) {
    return new ResponseEntity<>(service.parse(body), HttpStatus.OK);
  }
}
