package com.csssr.controller;

import com.csssr.service.model.ParseRequest;
import com.csssr.service.model.ParseSettings;
import io.restassured.http.ContentType;
import org.apache.http.HttpStatus;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;

/**
 * The negative integration tests for {@link com.csssr.web.controller.v1.ParseController}.
 *
 * @author Eugene Makarenko
 * @see com.csssr.web.controller.v1.ParseController
 * @see ParseControllerBaseIT
 */
public class NegativeParseControllerIT extends ParseControllerBaseIT {

  private void postRequestTemplate(Object body) {
    given()
        .log()
        .all()
        .body(body)
        .contentType(ContentType.JSON)
        .when()
        .post(getParsePath())
        .then()
        .log()
        .all()
        .statusCode(HttpStatus.SC_BAD_REQUEST);
  }

  @Test
  public void post__empty_body__statusCode400() {
    postRequestTemplate("");
  }

  @Test
  public void post__empty_settings__statusCode400() {
    postRequestTemplate(
        ParseRequest.builder()
            .settings(ParseSettings.builder().build())
            .stringToParse("stringToParse")
            .build());
  }

  @Test
  public void post__null_settings__statusCode400() {
    postRequestTemplate(
        ParseRequest.builder().settings(null).stringToParse("string to parse").build());
  }

  @Test
  public void post__empty_stringToParse__statusCode400() {
    postRequestTemplate(
        ParseRequest.builder()
            .settings(ParseSettings.builder().separator("-").minElementsCount(1L).build())
            .stringToParse("")
            .build());
  }

  @Test
  public void post__null_stringToParse__statusCode400() {
    postRequestTemplate(
        ParseRequest.builder()
            .settings(ParseSettings.builder().separator("-").minElementsCount(1L).build())
            .stringToParse(null)
            .build());
  }

  @Test
  public void post__empty_separator__statusCode400() {
    postRequestTemplate(
        ParseRequest.builder()
            .settings(ParseSettings.builder().separator("").minElementsCount(1L).build())
            .stringToParse("string to parse")
            .build());
  }

  @Test
  public void post__null_separator__statusCode400() {
    postRequestTemplate(
        ParseRequest.builder()
            .settings(ParseSettings.builder().separator(null).minElementsCount(1L).build())
            .stringToParse("string to parse")
            .build());
  }

  @Test
  public void post__null_minElementsCount__statusCode400() {
    postRequestTemplate(
        "{\n"
            + "    \"settings\": {\n"
            + "        \"separator\": \" \",\n"
            + "        \"minElementsCount\": null\n"
            + "    },\n"
            + "    \"stringToParse\": \"as ask\"\n"
            + "}");
  }

  @Test
  public void post__negative_minElementsCount__statusCode400() {
    postRequestTemplate(
        ParseRequest.builder()
            .settings(ParseSettings.builder().separator(" ").minElementsCount(-2L).build())
            .stringToParse("string to parse")
            .build());
  }

  @Test
  public void post__unknown_property__statusCode400() {
    postRequestTemplate(
        "{\n"
            + "    \"settings\": {\n"
            + "        \"unknown\": true,\n"
            + "        \"separator\": \" \",\n"
            + "        \"minElementsCount\": 1\n"
            + "    },\n"
            + "    \"stringToParse\": \"as ask\"\n"
            + "}");
  }
}
