package com.csssr.controller;

import com.csssr.service.model.Group;
import com.csssr.service.model.ParseRequest;
import com.csssr.service.model.ParseSettings;
import com.csssr.service.model.ParsedResult;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import org.apache.http.HttpStatus;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;

import static io.restassured.RestAssured.given;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * The positive integration tests for {@link com.csssr.web.controller.v1.ParseController}.
 *
 * @author Eugene Makarenko
 * @see com.csssr.web.controller.v1.ParseController
 * @see ParseControllerBaseIT
 */
public class PositiveParseControllerIT extends ParseControllerBaseIT {

  private void postRequestTemplate(ParseRequest body, ParsedResult result) {
    JsonPath responseAsJson =
        given()
            .log()
            .all()
            .body(body)
            .contentType(ContentType.JSON)
            .when()
            .post(getParsePath())
            .then()
            .log()
            .all()
            .statusCode(HttpStatus.SC_OK)
            .extract()
            .body()
            .jsonPath();

    ParsedResult actual = responseAsJson.getObject("", ParsedResult.class);
    assertEquals(result, actual);
  }

  @Test
  public void post__separator_space_minElementsCount_1_stringToParse_oneWorld__emptyResult() {
    // given
    ParseSettings settings = ParseSettings.builder().separator(" ").minElementsCount(1L).build();

    // when
    postRequestTemplate(
        ParseRequest.builder().settings(settings).stringToParse("oneWorld").build(),
        ParsedResult.builder().settings(settings).groups(Collections.emptyList()).build());
  }

  @Test
  public void post__separator_space_minElementsCount_1_stringToParse_6_worlds__2_groups_5_worlds() {
    // given
    ParseSettings settings = ParseSettings.builder().separator(" ").minElementsCount(1L).build();

    // when
    postRequestTemplate(
        ParseRequest.builder()
            .settings(settings)
            .stringToParse("сапог сарай арбуз болт бокс биржа")
            .build(),
        ParsedResult.builder()
            .settings(settings)
            .groups(
                Arrays.asList(
                    Group.builder()
                        .label('б')
                        .elements(Arrays.asList("биржа", "бокс", "болт"))
                        .build(),
                    Group.builder().label('с').elements(Arrays.asList("сапог", "сарай")).build()))
            .build());
  }

  @Test
  public void post__separator_dot_minElementsCount_2_stringToParse_9_worlds__3_groups_3_worlds() {
    // given
    ParseSettings settings = ParseSettings.builder().separator(".").minElementsCount(2L).build();

    // when
    postRequestTemplate(
        ParseRequest.builder()
            .settings(settings)
            .stringToParse("cat.man.curse.milk.dog.digger.dot.catch.map")
            .build(),
        ParsedResult.builder()
            .settings(settings)
            .groups(
                Arrays.asList(
                    Group.builder()
                        .label('c')
                        .elements(Arrays.asList("catch", "curse", "cat"))
                        .build(),
                    Group.builder()
                        .label('d')
                        .elements(Arrays.asList("digger", "dog", "dot"))
                        .build(),
                    Group.builder()
                        .label('m')
                        .elements(Arrays.asList("milk", "man", "map"))
                        .build()))
            .build());
  }
}
