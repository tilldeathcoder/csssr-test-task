package com.csssr.service.comparator;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ElementsComparatorTest {

  @Test
  public void compare__two_worlds_desc_order_by_length__success() {
    // given
    String world1 = "sunny";
    String world2 = "sand";

    // when
    List<String> expectedOrder = Arrays.asList(world1, world2);
    List<String> actualOrder = Arrays.asList(world1, world2);
    actualOrder.sort(new ElementsComparator());

    // then
    assertEquals(expectedOrder, actualOrder);
  }

  @Test
  public void compare__two_worlds_same_length_asc_order__success() {
    // given
    String world1 = "soak";
    String world2 = "sand";

    // when
    List<String> expectedOrder = Arrays.asList(world2, world1);
    List<String> actualOrder = Arrays.asList(world1, world2);
    actualOrder.sort(new ElementsComparator());

    // then
    assertEquals(expectedOrder, actualOrder);
  }

  @Test
  public void compare__three_worlds_same_length_asc_order__success() {
    // given
    String world1 = "soak";
    String world2 = "sand";
    String world3 = "sell";

    // when
    List<String> expectedOrder = Arrays.asList(world2, world3, world1);
    List<String> actualOrder = Arrays.asList(world1, world2, world3);
    actualOrder.sort(new ElementsComparator());

    // then
    assertEquals(expectedOrder, actualOrder);
  }
}
