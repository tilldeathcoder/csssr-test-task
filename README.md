## Тестовое задание для CSSSR по Java

### Описание:
Есть строка, состоящая из слов. Все слова в ней разделены одним пробелом. Нужно преобразовать строку в такую структуру данных, которая группирует слова по первой букве в слове. Затем вывести только группы, содержащие более одного элемента.

Группы должны быть отсортированы в алфавитном порядке. Слова внутри группы нужно сортировать по убыванию количества символов; если количество символов равное, то сортировать в алфавитном порядке.

Пример строки: String s = «сапог сарай арбуз болт бокс биржа»

Отсортированная строка: [б=[биржа, бокс, болт], c=[caпог, сарай]]

_Задание выполнено в виде REST сервиса. Есть возможность задать разделитель и минимальное количество элементов в группе._
***
### Технологии
* Java v13
* Spring Boot v2.2.5
* Gradle
* Swagger
***
### Сборка проекта и тесты
* Запуск проект: `gradlew bootRun`
* Сборка проекта: `gradlew build`
* Запустить интеграционные тесты: `gradlew integrationTest`
***
### Swagger
* Swagger UI: `http://localhost:8080/string-parser/swagger-ui.html`
* Swagger API: `http://localhost:8080/string-parser/v2/api-docs`
***
### Примеры:
### curl
```shell script
curl --header "Content-Type: application/json" \
  --request POST \
  --data '{"settings" : {"separator": " ","minElementsCount": 1},"stringToParse": "сапог сарай арбуз болт бокс биржа бок"}' \
  http://localhost:8080/string-parser/api/v1/parse
```
### Postman
* Метод: `POST`
* Путь: `http://localhost:8080/string-parser/api/v1/parse`
* Тело:
```json
{
    "settings": {
        "separator": " ",
        "minElementsCount": 1
    },
    "stringToParse": "сапог сарай арбуз болт бокс биржа бок"
}
```
* Ответ:
```json
{
    "settings": {
        "separator": " ",
        "minElementsCount": 1
    },
    "groups": [
        {
            "label": "б",
            "elements": [
                "биржа",
                "бокс",
                "болт",
                "бок"
            ]
        },
        {
            "label": "с",
            "elements": [
                "сапог",
                "сарай"
            ]
        }
    ]
}
```
### Docker
#### Linux
* Build image: `sudo docker build -f Dockerfile -t csssr .`
* Run container: `sudo docker run csssr`

