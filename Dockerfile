FROM ubuntu:18.04
MAINTAINER Eugene Makarenko <eugene.makarenko.work@gmail.com>

# Updating apt and install deependencies
RUN echo 'Updating apt' && \
apt-get update && \
apt-get install apt-utils -y && \
apt-get update && \
apt-get install software-properties-common -y && \
apt-get update && \
echo 'apt was successfully updated'

# Installing git
RUN echo 'Installing Git' && \
apt-get install git -y && \
echo 'Git was successfully installed' && \
git --version

# Installing Java 13
RUN echo 'Installing Java 13' && \
apt-get install gnupg2 -y && \
apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 0xB1998361219BD9C9 && \
apt-add-repository 'deb http://repos.azulsystems.com/ubuntu stable main' && \
apt-get update && \
apt install zulu-13 -y && \
echo 'Java 13 was successfully installed' && \
java -version

# Installing project
RUN echo 'Cloning project from git repository' && \
git clone https://gitlab.com/TillDeathCoder/csssr-test-task.git && \
echo 'Project was successfully cloned from git repository' && \
cd csssr-test-task && \
chmod +x gradlew && \
./gradlew build -x test

CMD java -jar csssr-test-task/build/libs/csssr-test-task-0.0.1-SNAPSHOT.jar